#!/usr/bin/env ruby
require 'httparty'
require 'json'
require 'base64'

class Jira
  def initialize
    @base_uri = 'https://msts-eng.atlassian.net/rest/api/latest'
    read_config
  end

  def strip_clone(issue)
    puts "For parent task #{issue}..."
    subtasks = get_subtask_list(issue)
    subtasks.each do |task|
      strip(task)
    end
  end

  private

  def read_config
    f = File.open( ENV['HOME'] + '/.jira', 'r')
    file = f.each_line.map { |x| x.strip }
    f.close
    #@auth = {:username => file[0], :password => file[1]}
    @auth = 'Basic ' + Base64.encode64(file[0])
  end

  def get_subtask_list (issue)
    headers = { "Authorization" => @auth }
    response = HTTParty.get(@base_uri + '/issue/' + issue, :headers => headers)
    tasks = response['fields']['subtasks']
    tasks.map do |task|
      {:key => task['key'], :summary => task['fields']['summary']}
    end
  end

  def strip (task)
    if /^CLONE - / =~ task[:summary]
      task[:cleaned] = task[:summary].gsub(/^(CLONE - )+/, '')
      send (task)
    else
      puts "#{task[:key]} - «#{task[:summary]}» will not be modified"
    end
  end

  def send (task)
    puts "#{task[:key]} - «#{task[:summary]}» ===> «#{task[:cleaned]}»"
    body = { "fields" => { "summary" => task[:cleaned] } }
    headers = { "Content-Type" => 'application/json', "Authorization" => @auth }
    response = HTTParty.put(@base_uri + '/issue/' + task[:key], :body => body.to_json, :headers => headers)
    puts "response - " + response.response.code.to_s
  end
end

jira = Jira.new
ARGV.each do |issue|
  jira.strip_clone issue
end
