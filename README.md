unclone
=======
This is a small utility that will remove the string "CLONE - " from 
summary of any MSTS jira subtasks.

Installation
------------
```
bundle install --path=vendor/bundle
```

Usage
-----
Before you can use this tool, you will need to get your JIRA API token from https://id.atlassian.com/manage/api-tokens

`unclone` will look for your Jira credentials in `~/.jira`.  The file should
look something like
```
khayakawa@msts.com:jiraapitoken
```

Then you can run
```
bundle exec ruby unclone FC-1234 FC-1246
```
When you run the program as above, the script will look for all subtasks
that belong to those two parent issues.  If the subtask's summary starts with
"CLONE - ", it will remove that part of the text from the summary.  If the
task summary starts with "CLONE - CLONE - " etc, all instances of "CLONE - "
at the beginning of summary will be removed.

To make this command available anywhere while you're logged in, use this alias
(thanks to @jwmarrs for the tip)
```
alias unclone="BUNDLE_GEMFILE=$HOME/projects/unclone/Gemfile bundle exec $HOME/projects/unclone/unclone.rb"
```
